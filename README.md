# Wine TKG Changer

This project was just for my own convenience, as I built a bunch of different versions of Wine-TKG, then held them as folders in my /usr/local/bin/ directory to then change the wine_binary variable's file path, within my Grapejuice config file.

## REQUIRED

- Bash
- Wine Dependencies
- GTK3
- Python


If you want to actually build this project and run it, first you need a user named `"aizakku"` then you need a Folder in your ``/usr/local/bin/`` directory, and call it "Custom_Wine" then, create another folder within `"Custom_Wine"` and call it `"Wine_Builds.v.2"`

If you wish to actually switch in between wine versions and it actually work, firstly you will not need to do any of this as upon release of Grapejuice version 4 it did all of this essentially for you.

But, if you do want it to work how it's supposed to then

you will first have to install Grapejuice version 3.xx then finally, build the following wine-tkg's and move them into this directory ``/usr/local/bin/Custom_Wine/Wine_Builds.v.2/"``

Grapejuice Link: [Grapejuice Release 4.14.2](https://gitlab.com/brinkervii/grapejuice/-/releases/v4.14.2)

## Needed Wine-TKG Version's
- Wine 6.7
- Wine 6.9
- Wine 6.14
- Wine 6.15

**Create These Directories and name them as what is seen below:**

## All Required WINE-TKG Root Directory Names

- 6.7_WineTKG
- 6.9_WineTKG
- 6.14_WineTKG
- 6.15_WineTKG

## All Required WINE-TKG Build Types Under The Wine-TKG Root Directories ex. 6.x_WineTKG

- Wine_
- Wine_Proton
- Wine_Staging
- Wine_Staging_Protonified

This was to ensure we could switch between all of the different versions of my built wine-tkgs to then test all of them with Roblox Studio and the Roblox Client.

To achieve all of this you will first need to edit a configuration file that [Wine-TKG](https://github.com/Frogging-Family/wine-tkg-git.git) uses to build wine-tkgs, then adjust settings accordingly. For example if we wanted to make a version 6.7 wine-tkg build that uses Wine Staging, then the path should look something like this:

``/usr/local/bin/Custom_Wine/Wine_Builds.v.2/6.7_WineTKG/Wine_Staging"``

the wine bin path should look like this

"``/usr/local/bin/Custom_Wine/Wine_Builds.v.2/6.7_WineTKG/Wine_Staging/wine/bin"``"

A guide to building wine-tkg-git for Roblox can be found here: [Guide](https://github.com/e666666/robloxWineBuildGuide#steps-to-compile)

**Although, all of this is basically deprecated and unnecessary now by just updating from Grapejuice 3.xx to 4.xx!**


## Socials

LinkedIn: [Check Out my LinkedIn!](https://www.linkedin.com/in/delbertvjr)
Twitter: [Check Out my Twitter!](https://twitter.com/AizakkuZ)