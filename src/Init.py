import gi

gi.require_version("Gtk", "3.0")
gi.require_version('Notify', '0.7')

from gi.repository import Gtk
from gi.repository import Notify


WINE_GTK_BUILDS_PATH = "/usr/local/bin/Custom_Wine/Wine_Builds.v.2/"
BASHRC_PATH = "/home/aizakku/.bashrc"
GRAPEJUICE_USER_SETTINGS_PATH = "/home/aizakku/.config/brinkervii/grapejuice/user_settings.json"

SELECTED_WINE_VERSION = 6.7
WINE_HOLDER_SELECTED = "/usr/local/bin/Custom_Wine/Wine_Builds.v.2/" + str(SELECTED_WINE_VERSION) + "_WineTKG/"
WINE_BIN = WINE_HOLDER_SELECTED + "/bin"
WINE_EXEC = WINE_BIN + "/wine"

IS_WINE_STAGING = False
IS_WINE_PROTONIFIED = False
WINDOW = Gtk.Window


def GrabWineBin(WINE_HOLDER_SELECTED):
     New_Prefix = WINE_HOLDER_SELECTED + "Wine_"

     if IS_WINE_STAGING and IS_WINE_PROTONIFIED:
         New_Prefix = New_Prefix + "Staging_Protonified"
     elif IS_WINE_STAGING:
         New_Prefix = New_Prefix + "Staging"
     elif IS_WINE_PROTONIFIED:
         New_Prefix = New_Prefix + "Proton"
     return New_Prefix

def Set_Wine_Path(SELECTED_WINE_VERSION):
   
   WINE_HOLDER_SELECTED = "/usr/local/bin/Custom_Wine/Wine_Builds.v.2/" + str(SELECTED_WINE_VERSION) + "_WineTKG/"
   WINE_BIN = WINE_HOLDER_SELECTED + "/bin" 

   WINE_HOLDER_SELECTED = GrabWineBin(WINE_HOLDER_SELECTED)
   WINE_BIN = WINE_HOLDER_SELECTED + "/bin"
   WINE_EXEC = WINE_BIN + "/wine"


   BASH_RC = open(BASHRC_PATH, "r")
   USER_SETTINGS = open(GRAPEJUICE_USER_SETTINGS_PATH, "r")


   B_Lines = BASH_RC.readlines()
   U_Lines = USER_SETTINGS.readlines()


   U_Lines[2] = '   "wine_binary": ' + '"' + WINE_EXEC + '",' + "\n"
   B_Lines[119] = 'export PATH=$PATH:' + WINE_BIN + "\n"

   Wine_Binary = U_Lines[2]
   Bin_Path = B_Lines[119]

   BASH_RC = open(BASHRC_PATH, "w")
   USER_SETTINGS = open(GRAPEJUICE_USER_SETTINGS_PATH, "w")

   BASH_RC.writelines(B_Lines)
   USER_SETTINGS.writelines(U_Lines)
   BASH_RC.close(); USER_SETTINGS.close()


   print( "BINARY WINE ASSIGNED:", Wine_Binary)
   print( "PREFIX PATH ASSIGNED:", Bin_Path)


class MyWindow(WINDOW):
    def __init__(self):
        Gtk.Window.__init__(self, title="Wine TKG Changer v.0.1")
        Gtk.Window.set_default_size(self, 720, 50)
        Notify.init("Wine TKG Changer v.0.1")

        Section = Gtk.Box(spacing=5)
        header = Gtk.HeaderBar()
        self.add(Section)
        self.set_border_width(10) 

        Section.set_halign(Gtk.Align.CENTER)
        Section.set_valign(Gtk.Align.START)


        proton = Gtk.Switch()
        staging = Gtk.Switch()
        confirm = Gtk.Button(label="Confirm")
        label = Gtk.Label(label="Version Selection |")


        header = Gtk.HeaderBar()
        header.set_show_close_button(False)
        header.props.title = "Wine TKG Changer v.0.1"
        self.set_titlebar(header)


       
        button_6_7 = Gtk.RadioButton.new_with_label_from_widget(None, "Wine 6.7")
        button_6_9 = Gtk.RadioButton.new_with_label_from_widget(button_6_7, "Wine 6.9")
        button_6_14 = Gtk.RadioButton.new_with_label_from_widget(button_6_7, "Wine 6.14")
        button_6_15 = Gtk.RadioButton.new_with_label_from_widget(button_6_7, "Wine 6.15")
                                                    
        #button_6_7.set_halign(Gtk.Align.CENTER); button_6_7.set_valign(Gtk.Align.CENTER)

        button_6_7.connect("toggled", self.on_button_toggled, "6.7")
        button_6_9.connect("toggled", self.on_button_toggled, "6.9")
        button_6_14.connect("toggled", self.on_button_toggled, "6.14")
        button_6_15.connect("toggled", self.on_button_toggled, "6.15")

        proton.connect("notify::active", self.on_proton_switch_activated)
        staging.connect("notify::active", self.on_staging_switch_activated)
        confirm.connect("clicked", self.on_button_clicked)


        Section.pack_start(label, True, True, 0)
        Section.pack_start(button_6_7, False, False, 0)
        Section.pack_start(button_6_9, False, False, 0)
        Section.pack_start(button_6_14, False, False, 0)
        Section.pack_start(button_6_15, False, False, 0)
        Section.pack_start(confirm, True, True, 0)
        Section.pack_start(staging, True, True, 0)
        Section.pack_start(proton, True, True, 0)



    def check_enabled(self, button):
         if button.get_active():
            state = "on"

    def on_button_toggled(self, button, name):
        if button.get_active():
            global SELECTED_WINE_VERSION
            state = "on"

            SELECTED_WINE_VERSION = float(name)
        else:
            state = "off"

    def on_proton_switch_activated(self, switch, gparam):
        global IS_WINE_PROTONIFIED 

        if switch.get_active():
            state = "on"
            IS_WINE_PROTONIFIED = True
        else:
            state = "off"
            IS_WINE_PROTONIFIED = False  

    def on_staging_switch_activated(self, switch, gparam):
        global IS_WINE_STAGING 

        if switch.get_active():
            state = "on"
            IS_WINE_STAGING = True
        else:
            state = "off"
            IS_WINE_STAGING = False

    def on_button_clicked(self, widget):
        
        n = Notify.Notification.new("Wine TKG Changer v.0.1", "Wine Has Been Changed To Version " + str(SELECTED_WINE_VERSION))

        if IS_WINE_STAGING and IS_WINE_PROTONIFIED:
            n = Notify.Notification.new("Wine TKG Changer v.0.1", "Wine Has Been Changed To Version " + str(SELECTED_WINE_VERSION) + " Staging Protonified")
        elif IS_WINE_STAGING:
            n = Notify.Notification.new("Wine TKG Changer v.0.1", "Wine Has Been Changed To Version " + str(SELECTED_WINE_VERSION) + " Staging")
        elif IS_WINE_PROTONIFIED:
            n = Notify.Notification.new("Wine TKG Changer v.0.1", "Wine Has Been Changed To Version " + str(SELECTED_WINE_VERSION) + " Proton")


        Set_Wine_Path(SELECTED_WINE_VERSION)
        Gtk.main_quit()
        n.show()    
       

win = MyWindow()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()